<html>

<head>
    <title>First Project</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" /> -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700|Roboto:400,400i,700,700i" />
    <link rel="stylesheet" href="styles/style.css" />

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
    <link rel="icon" href="images/Logo.png">
    <script src="validations.js"></script>
</head>

<body class="loggedin">
    <header class="header">
        <a href="index.php" class="logo">G&A</a>
        <input class="menu-btn" type="checkbox" id="menu-btn" />
        <label class="menu-icon" for="menu-btn"><span class="navicon"></span></label>
        <ul class="menu">
            <li> <a href="index.php">Home</a></li>
            <li> <a href="pages/news/news.php">News</a></li>
            <li> <a href="pages/Gallery/gallery.php">Gallery</a></li>
            <li> <a href="pages/Contactus/index.php">Contact Us</a></li>
            <li> <a href="pages/aboutus/index.php">About Us</a></li>
        </ul>
    </header>
    <div class="container">
        <section class="banner">
            <div class="inner">
                <h2>First Project in HTML, CSS, JS, PHP</h2>
                <ul class="actions">
                    <li><a class="about-button big special" href="#about-section">About Us</a></li>
                    <li><a class="signup-button big alt" href="#contact-wrapper">Contact Us</a></li>
                </ul>
            </div>
        </section>

        <div id="news" class="news-wrapper">
            <?php
            require_once('admin/db.php');
            $result = $conn->prepare("SELECT * FROM quotes ORDER BY id DESC");
            $result->execute();
            for ($i = 0; $row = $result->fetch(); $i++) {
                $id = $row['id'];
            ?>

                <div class="inner-news">

                    <div class="new-content">


                        <div class="item-news">

                            <h3><?php echo $row['title']; ?></h3>
                            <div name="content" class="paragraph">
                                <p><?php echo $row['content']; ?></p>
                            </div>
                            <button name="submit" href="#" class="button">More</button>
                        </div>
                    </div>
                </div>
            <?php } ?>

        </div>

    </div>
    <div>
        <footer>
            <div class="footer-content">
                <div class="logo-container">
                    <img alt="logo" src="Logo.png">
                </div>
                <ul class="social-media">
                    <li><a href="https://www.facebook.com/alr.r9"><i class="fab fa-facebook" aria-hidden="true"></i></a>
                    </li>
                    <li><a href="https://www.instagram.com/gramozmakolli/?hl=en"><i class="fab fa-instagram"></i></a></li>
                    <li><a href="https://www.youtube.com/channel/UCPXzm-FKM9cMaKhMrusfuCA?"><i class="fab fa-youtube" aria-hidden="true"></i></a></li>
                    <li><a href="mailto:albionr9224@gmail.com"><i class="fas fa-envelope-square"></i></a>
                    </li>
                    <li><a href="https://twitter.com/gramozmakolli"><i class="fab fa-twitter" aria-hidden="true"></i></a>
                    </li>
                </ul>
                <div class="footer-bottom">
                    <p>copyright &copy;2020 GM & AR</p>
                </div>
            </div>
        </footer>
    </div>


</body>


</html>