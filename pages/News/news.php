<html>

<head>
    <title>News!</title>

    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700|Roboto:400,400i,700,700i" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles/font-awesome.min.css" />
    <link rel="icon" href="../../images/Logo.png">
    <script src="../News/subsvalidate.js"></script>

</head>
<!-- BODY -->

<body>
    <header class="header">
        <a href="../../index.php" class="logo">G&A</a>
        <input class="menu-btn" type="checkbox" id="menu-btn" />
        <label class="menu-icon" for="menu-btn"><span class="navicon"></span></label>
        <ul class="menu">
            <li><a href="../../index.php">Home</a></li>
            <li><a href="../News/news.php">News</a></li>
            <li><a href="../Gallery/gallery.php">Gallery</a></li>
            <li> <a href="../Contactus/index.php">Contact Us</a></li>
            <li> <a href="../aboutus/index.php">About Us</a></li>
        </ul>
    </header>
    <!-- Container -->
    <div class="row">
        <?php
        require_once('../../admin/db.php');
        $result = $conn->prepare("SELECT * FROM posts ORDER BY tbl_image_id DESC");
        $result->execute();
        for ($i = 0; $row = $result->fetch(); $i++) {
            $id = $row['tbl_image_id'];
        ?>
            <div class="news-suite-block-container">
                <div class="news-suite-flex">
                    <div class="news-suite-item">
                        <div class="image-container">
                            <?php if ($row['image_location'] != "") : ?>
                                <img src="../../images/posts/<?php echo $row['image_location']; ?>">
                            <?php else : ?>
                                <img src="../../images/posts/empty.png">
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="news-suite-item">
                        <div class="content-container">
                            <div class="news-suite-content">
                                <h3 class="news-suite-headline" name="title" style="color: #000F0C" require><?php echo $row['title']; ?></h3>
                                <div class="news-suite-bodycopy" style="color:#000F0C">
                                    <p name="content-of-news" require><?php echo $row['content']; ?></p>
                                </div>

                                <div class="news-suite__cta">


                                    <a href="#" class="button">LEARN MORE</a>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <form method="post" method="myform" onsubmit="return validateForm();">
            <div class="subscribe">
                <h2>Subscribe to our mailing list</h2>
                <div class="subs-email">
                    <label for="email">Email Address</label>
                    <input type="email" name="email" id="email" placeholder="Email adresse" required>

                </div>
                <div class="subscribe-button">
                    <div class="submit">
                        <button type="submit" name="submit" class="button"> Submit </button>
                    </div>
                </div>
            </div>
        </form>

    </div>

    <!-- Footer -->
    <footer>
        <div class="footer-content">
            <div class="logo-container">
                <img alt="logo" src="../../images/Logo.png">
            </div>
            <ul class="social-media">
                <li><a href="https://www.facebook.com/alr.r9"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                </li>
                <li><a href="https://www.instagram.com/gramozmakolli/?hl=en"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                <li><a href="https://www.youtube.com/channel/UCPXzm-FKM9cMaKhMrusfuCA?"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                <li><a href="mailto:albionr9224@gmail.com"><i class="fa fa-envelope" aria-hidden="true"></i></a>
                </li>
                <li><a href="https://twitter.com/gramozmakolli"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                </li>
            </ul>

            <div class="footer-bottom">
                <p>copyright &copy;2020 GM & AR</p>
            </div>
        </div>
    </footer>
</body>
<?php
include_once '../../admin/database.php';
// If the user is not logged in redirect to the login page...

if (isset($_POST['submit'])) {
    $email = $_POST['email'];
    $sql = "INSERT INTO subscribe (email)
        VALUES ('$email')";
    if (mysqli_query($con, $sql)) {
        echo "Error: " . $sql . "
        " . mysqli_error($con);
    }
    mysqli_close($con);
    echo "<script>
            alert('Successfully Added!!!');
            window.location = 'news.php'
        </script>";
}
?>


</html>