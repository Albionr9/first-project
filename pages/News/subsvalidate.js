const email = document.getElementById('email')
const form = document.getElementById('form')
const errorElement = document.getElementById('errorElement')
form.addEventListener('submit', (e) => {
    let messages = []

    if (email.type === "email") {
        const re = /\S+@\S+\.\S+/
        if (re.test(email.value)) {
            messages.push('')
        } else {
            messages.push('Please enter valid email address')
        }
    }

})      