<html>

<head>
    <title>First Project</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" /> -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700|Roboto:400,400i,700,700i" />
    <link rel="stylesheet" href="../../styles/style.css" />

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
    <link rel="icon" href="../../images/Logo.png">
    <script src="validations.js"></script>
</head>

<body class="loggedin">
    <header class="header">
        <a href="index.php" class="logo">G&A</a>
        <input class="menu-btn" type="checkbox" id="menu-btn" />
        <label class="menu-icon" for="menu-btn"><span class="navicon"></span></label>
        <ul class="menu">
            <li> <a href="../../index.php">Home</a></li>
            <li> <a href="../news/news.php">News</a></li>
            <li> <a href="../Gallery/gallery.php">Gallery</a></li>
            <li> <a href="../Contactus/index.php">Contact Us</a></li>
            <li> <a href="../aboutus/index.php">About Us</a></li>
        </ul>
    </header>
    <div class="container">




        <div id="about-section">
            <div class="inner-container">
                <h1>About Us</h1>
                <p class="text">
                    Welcome to GM & AR, your number one source for all things programming. We're dedicated to giving you
                    the
                    very best of programming, with a focus on coding, developing, photoshop & illustrator.


                    Founded in 2020 by Gramoz Makolli and Albion Ramadani, GM & AR has come a long way from its
                    beginnings
                    in Prishtina. When they first started out, their passion for coding drove them to do tons of
                    research
                    and studying, so that GM & AR can offer you the best programming experience. We now serve customers
                    all
                    over Kosova, and are thrilled that we're able to turn our passion into our own website.


                    We hope you enjoy our products as much as we enjoy offering them to you. If you have any questions
                    or
                    comments, please don't hesitate to contact us.


                    Sincerely,
                    Gramoz Makolli & Albion Ramadani
                </p>
                <div class="skills">
                    <span>Web Design</span>
                    <span>Photoshop & Illustrator</span>
                    <span>Coding</span>
                </div>
            </div>
        </div>
        <footer>
            <div class="footer-content">
                <div class="logo-container">
                    <img alt="logo" src="../../images/Logo.png">
                </div>
                <ul class="social-media">
                    <li><a href="https://www.facebook.com/alr.r9"><i class="fab fa-facebook" aria-hidden="true"></i></a>
                    </li>
                    <li><a href="https://www.instagram.com/gramozmakolli/?hl=en"><i class="fab fa-instagram"></i></a></li>
                    <li><a href="https://www.youtube.com/channel/UCPXzm-FKM9cMaKhMrusfuCA?"><i class="fab fa-youtube" aria-hidden="true"></i></a></li>
                    <li><a href="mailto:albionr9224@gmail.com"><i class="fas fa-envelope-square"></i></a>
                    </li>
                    <li><a href="https://twitter.com/gramozmakolli"><i class="fab fa-twitter" aria-hidden="true"></i></a>
                    </li>
                </ul>
                <div class="footer-bottom">
                    <p>copyright &copy;2020 GM & AR</p>
                </div>
            </div>
        </footer>
    </div>


</body>


</html>