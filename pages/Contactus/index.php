<html>

<head>
    <title>First Project</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" /> -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700|Roboto:400,400i,700,700i" />
    <link rel="stylesheet" href="../../styles/style.css" />

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
    <link rel="icon" href="../../images/Logo.png">
    <script src="validations.js"></script>
</head>

<body class="loggedin">
    <header class="header">
        <a href="index.php" class="logo">G&A</a>
        <input class="menu-btn" type="checkbox" id="menu-btn" />
        <label class="menu-icon" for="menu-btn"><span class="navicon"></span></label>
        <ul class="menu">
            <li> <a href="../../index.php">Home</a></li>
            <li> <a href="../news/news.php">News</a></li>
            <li> <a href="../Gallery/gallery.php">Gallery</a></li>
            <li> <a href="index.php">Contact Us</a></li>
            <li> <a href="../aboutus/index.php">About Us</a></li>
        </ul>
    </header>
    <div id="contact-wrapper">
        <div id="contact" class="contact-content">

            <div class="content">


                <!-- <form> meth -->
                <form method="post" method="myform" onsubmit="return validateForm();">
                    <div class="rows">
                        <div class="contact-tittle">
                            <h2>Contact us</h2>
                        </div>
                        <div id="error_message"></div>
                        <div class="first-row">
                            <div class="row">
                                <input type="text" class="text" id="name" placeholder="Name" name="name" required>
                                <input type="text" class="text" id="email" placeholder="Email" name="email">
                            </div>
                            <div class="description">
                                <textarea id="message" placeholder="Message" class="mes sage" name="message" required></textarea>
                            </div>
                        </div>
                        <div class="row-button">
                            <div class="submit">
                                <input type="submit" name="save" id="submit" class="contact-button" required></input>
                            </div>
                        </div>
                    </div>


            </div>
        </div>
    </div>


    <footer>
        <div class="footer-content">
            <div class="logo-container">
                <img alt="logo" src="../../images/Logo.png">
            </div>
            <ul class="social-media">
                <li><a href="https://www.facebook.com/alr.r9"><i class="fab fa-facebook" aria-hidden="true"></i></a>
                </li>
                <li><a href="https://www.instagram.com/gramozmakolli/?hl=en"><i class="fab fa-instagram"></i></a></li>
                <li><a href="https://www.youtube.com/channel/UCPXzm-FKM9cMaKhMrusfuCA?"><i class="fab fa-youtube" aria-hidden="true"></i></a></li>
                <li><a href="mailto:albionr9224@gmail.com"><i class="fas fa-envelope-square"></i></a>
                </li>
                <li><a href="https://twitter.com/gramozmakolli"><i class="fab fa-twitter" aria-hidden="true"></i></a>
                </li>
            </ul>
            <div class="footer-bottom">
                <p>copyright &copy;2020 GM & AR</p>
            </div>
        </div>
    </footer>
    </div>


</body>
<?php
include_once '../../admin/database.php';
// If the user is not logged in redirect to the login page...

if (isset($_POST['save'])) {
    $name = $_POST['name'];
    $email = $_POST['email'];
    $message = $_POST['message'];
    $sql = "INSERT INTO contact_us (name,email,message)
                VALUES ('$name','$email','$message')";
    if (mysqli_query($con, $sql)) {
        echo "Error: " . $sql . "
            " . mysqli_error($con);
    }
    mysqli_close($con);
    echo "<script>alert('Successfully Added!!!'); window.location='index.php'</script>";
}
?>

</html>