<?php
// We need to use sessions, so you should always start sessions using the below code.
session_start();
// If the user is not logged in redirect to the login page...
if (!isset($_SESSION['loggedin'])) {
    header('Location: ../login/index.php');
    exit;
}
?>
<html>

<head>
    <meta charset="utf-8">
    <title>New Quotes Page</title>
    <link href="../style.css" rel="stylesheet" type="text/css">
    <link href="../posts/new_post/style.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
</head>

<body class="loggedin">
    <header class="header">
        <a href="../home.php" class="logo">Dashboard Admin</a>
        <input class="menu-btn" type="checkbox" id="menu-btn" />
        <label class="menu-icon" for="menu-btn"><span class="navicon"></span></label>
        <ul class="menu">

            <li> <a href="../home.php"><i class="fas fa-home"></i>Home</a></li>
            <li> <a href="../admins/index.php"><i class="fas fa-user-circle"></i>Admins</a> </li>
            <li> <a href="../../gallery/index.php"><i class="fas fa-image"></i>Gallery</a> </li>
            <li> <a href="../posts/index.php"><i class="fas fa-newspaper"></i>Posts</a> </li>
            <li> <a href="../profile.php"><i class="fas fa-user-circle"></i>Profile</a> </li>
            <li> <a href="../logout.php"><i class="fas fa-sign-out-alt"></i>Logout</a> </li>
        </ul>
    </header>
    <div class="content">
        <h2>Add New Quotes</h2>

        <form method="post" action="addnew.php">
            <div class="row">
                <div class="col-25">
                    <label for="fname">Title</label>
                </div>
                <div class="col-75">
                    <input type="text" id="title" name="title" placeholder="Title.." required>
                </div>

                <div class="row">
                    <div class="col-25">
                        <label for="subject">Content</label>
                    </div>
                    <div class="col-75">
                        <textarea id="subject" name="content" placeholder="Write something.." style="height:200px" required></textarea>
                    </div>

                </div>
                <div class="row">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                    <button type="submit" name="Submit" class="btn btn-primary">ADD</button>
                </div>
        </form>
    </div>
</body>


</html>