<?php
include("../header.php");
// We need to use sessions, so you should always start sessions using the below code.
session_start();
// If the user is not logged in redirect to the login page...
if (!isset($_SESSION['loggedin'])) {
    header('Location: ../login/index.php');
    exit;
}
?>

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <title>Post Page</title>
    <link href="../style.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
</head>

<body class="loggedin">

    <div class="content">
        <h2>Posts Page</h2>



        <button type="button" class="close" data-dismiss="alert">
            <a href="new_post/new-post.php"><i class="fab fa-usps"></i>Add New Post</a>
        </button>
        <!-- <form class="removeposts" method="post" action="delete.php">
            <input type="text" placeholder="Enter the post ID to delete" name="getdeleteid" required>
            <button type="submit">Remove post by ID </button>
        </form> -->
        <div>

            <table cellpadding="0" cellspacing="0" border="1" class="table table-striped table-bordered" id="example">
                <thead>
                    <tr>
                        <th style="text-align:center;">Id</th>
                        <th style="text-align:center;">Image</th>
                        <th style="text-align:center;">Title</th>
                        <th style="text-align:center;">Content</th>
                        <th style="text-align:center;">Delete</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    require_once('../db.php');
                    $result = $conn->prepare("SELECT * FROM posts ORDER BY tbl_image_id DESC ");
                    $result->execute();
                    for ($i = 0; $row = $result->fetch(); $i++) {
                        $id = $row['tbl_image_id'];
                    ?>
                        <tr>
                            <td style="text-align:center; word-break:break-all; width:300px;"> <?php echo $row['tbl_image_id']; ?></td>
                            <td style="text-align:center; margin-top:10px; word-break:break-all; width:450px; line-height:100px;">
                                <?php if ($row['image_location'] != "") : ?>
                                    <img src="../../images/posts/<?php echo $row['image_location']; ?>" width="100px" height="100px" style="border:1px solid #333333;">
                                <?php else : ?>
                                    <img src="../../images/posts/empty.png" width="100px" height="100px" style="border:1px solid #333333;">
                                <?php endif; ?>
                            </td>
                            <td style="text-align:center; word-break:break-all; width:300px;"> <?php echo $row['title']; ?></td>
                            <td style="text-align:center; word-break:break-all; width:500px;"> <?php echo $row['content']; ?></td>
                            <td style="text-align:center; word-break:break-all;"><a href="delete.php?tbl_image_id=<?php echo $row["tbl_image_id"]; ?>"><i class="far fa-trash-alt"></i></a></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</body>

</html>