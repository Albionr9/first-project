<?php
include("../header.php");
// We need to use sessions, so you should always start sessions using the below code.

session_start();
// If the user is not logged in redirect to the login page...
if (!isset($_SESSION['loggedin'])) {
	header('Location: ../login/index.php');
	exit;
}
?>

<html>

<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta charset="utf-8">
	<title>Subscribe Page</title>
	<link href="../style.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
</head>

<body class="loggedin">

	<div class="content">

		<h2>Subscribe Page</h2>

		<div class="content-1">

			<table cellpadding="0" cellspacing="0" border="1" class="table table-striped table-bordered" id="example">

				<thead>
					<tr>
						<th style="text-align:center;">Id</th>
						<th style="text-align:center;">Email</th>
						<th style="text-align:center;">Delete</th>

					</tr>
				</thead>
				<tbody>
					<?php
					require_once('../db.php');
					$result = $conn->prepare("SELECT * FROM subscribe ORDER BY id DESC");
					$result->execute();
					for ($i = 0; $row = $result->fetch(); $i++) {
						$id = $row['id'];
					?>
						<tr>
							<td style="text-align:center; word-break:break-all; width:300px;"> <?php echo $row['id']; ?></td>

							<td style="text-align:center; word-break:break-all; width:500px;"> <?php echo $row['email']; ?></td>
							<td style="text-align:center; word-break:break-all; "><a href="delete.php?id=<?php echo $row["id"]; ?>"><i class="far fa-trash-alt"></i></a></td>

						</tr>
					<?php } ?>
				</tbody>
			</table>

		</div>
	</div>
</body>

</html>